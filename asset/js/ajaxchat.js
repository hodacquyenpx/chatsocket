$(document).ready(function(){
  // kết nối socket
  var socket = io.connect( 'http://localhost:8080' );
  // lấy id đang đăng nhập
  var CurUser_id = $('.current_id').data("id");
  // khởi tạo clickid
  var ClickUser_id = 0;
  // khởi tạo room
  var room = 0;
  //khoi tao thoi gian hien tai
  function timecur () {
    now = new Date();
    year = "" + now.getFullYear();
    month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
    day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
    hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
    minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
    second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
  }
  // khởi tạo biến đếm count
  var count = 0;
  var ClickUsername = '';
  // khi class connect click
  if(CurUser_id > 0 ){
    socket.emit("client-send-username",CurUser_id);
  }
  socket.on('server-send-list-user',function(data){
    $('.useronline').html('');
    data.forEach(function(i){
      $('.on-off').each(function(index){
        console.log($(this).data('id'));
          if ($(this).data('id') == i) {
            $(this).html('on');
          }
      });
    });
  });


  $(".connect").click(function(){
    $('.panel-primary').addClass('block');
    // cộng count thêm 1
    count = count + 1;
    // gán biến ClickUser_id = id khi ta click vào class connect
    ClickUser_id = $(this).data("id");
    ClickUsername = $(this).data("username");
    // kiểm tra curid và clickid cái nào lớn hơn và nhỏ hơn để tạo room
    if(CurUser_id >= ClickUser_id){
      room_id1 = ClickUser_id;
      room_id2 = CurUser_id;
      room_chat = "Room"+"-"+ClickUser_id+"-"+CurUser_id;
    // tương tự như trên
    } else{
      room_id1 = CurUser_id;
      room_id2 = ClickUser_id;
      room_chat = "Room"+"-"+CurUser_id+"-"+ClickUser_id;
    }

    // sử dụng ajax kiểm tra xem room_chat đã tồn tại chưa nếu có rồi thì show ra listmessage
    $.ajax({
      url: base_url+'chat/checkRoom',
      type: 'post',
      dataType: 'json',
      data: {'room_id1':CurUser_id, 'room_id2':ClickUser_id, 'room_chat':room_chat},
      success: function(result){
        // gán room = kết quả trả về khi checkRoom ở controller Chat
        room = result.status;
        if(room == 1){
          alert('Bạn đã vào phòng của: ' + result.roomchat);
        }
        // kiểm tra xem đây là lần click đầu tiên thì dùng join room
        if(room == 1  && count == 1){
          listmess(result.list_mess);
          socket.emit('join_room',result);
        // ngược lại thì dùng change_room
        }else if(room == 1  && count > 1){
          listmess(result.list_mess);
          socket.emit('change_room',result);
        // nếu room == 0 có nghĩa là chưa có room_chat trong database
        // nên ta sẽ dùng hàm addRoom để tạo database
        }else if(room == 0){
          addRoom();
        }
      },
      error: function(err){
      }
    });
    // Addroom vào database
    function addRoom() {
      $.ajax({
        url: base_url+'chat/addRoom',
        type: 'post',
        dataType: 'json',
        data: {'room_id1':room_id1, 'room_id2':room_id2, 'CurUser_id':CurUser_id, 'ClickUser_id':ClickUser_id, 'room_chat':room_chat},
        success: function(result){

          if(result.status == 1){
            alert('Bạn đã vào phòng của: ' + result.roomchat);
          }
          //kiểm tra đây là lần click đầu tiên thì là join room
          if( count == 1){
            socket.emit('join_room',result);
            $(".ms").html('');
          // ngược lại nếu là lần click thứ 2 trở đi thì sẽ là change_room
          }else{
            socket.emit('change_room',result);
            $(".ms").html('');
          }
        },
        error: function(err){
        }
      });
    }
    // function này dùng khi checkroom mà có trong database và lôi dữ liệu message đã chat
    // trong database ra và in vào .chatlist
    function listmess(data){
      // kiểm tra có dữ liệu k
      if(data){
        $(".chatlist").html('');
        data.map(function (item){
          if(item.to_id == CurUser_id){
            $(".chatlist").append(
              `<li class='right clearfix'>
                 <span class='chat-img pull-right'>
                  <img src="//placehold.it/50/FA6F57/fff&amp;text=ME" alt="User Avatar" class="img-circle">
                 </span>
                 <div class='chat-body clearfix'>
                    <div class='header'>
                      <small class=' text-muted'>
                        <span class='glyphicon glyphicon-time'></span>
                        <span class='send_at'>${item.created}</span>
                      </small>
                      <strong class='pull-right primary-font username'>
                        ${item.username_to}
                      </strong>
                    </div>
                    <p class='message'>
                      ${item.message}
                    </p>
                 </div>
              </li>`

            );

          }else{
            $(".chatlist").append(
              `<li class='left clearfix'>
                <span class='chat-img pull-left'>
                  <img src='//placehold.it/50/55C1E7/fff&amp;text=STAFF' alt='User Avatar' class='img-circle'>
                </span>
                <div class='chat-body clearfix'>
                    <div class='header'>
                        <strong class='primary-font username'>
                          ${item.username_to}
                        </strong>
                        <small class='pull-right text-muted'>
                          <span class='glyphicon glyphicon-time'></span>
                          <span class='send_at'>
                            ${item.created}
                          </span>
                        </small>
                    </div>
                    <p class='message'>${item.message}</p>
                </div>
              </li>`
          );

          }
        });
      }else{
        // k có dữ liệu có nghĩa là room đó đã có trong database nhuwgn chưa có dữ liệu
        $(".chatlist").html('');
      }
    }
  });
  // Submit vào form và lấy dữ liệu của message
  $(".formmess").submit(function(){
    event.preventDefault();
    // message là dữ liệu của
    var message = {
      text: $("#message").val(),
      username : username,
      created : timecur()
    };
    // kiểm tra có dữ liệu trong input chat có kí tự > 0
    if($("#message").val().length > 0){
      // thì send message qua socket
      socket.emit("send_mesage",message);
      // sử dụng ajax để lưu message
      $.ajax({
        url: base_url+'chat/addMess',
        type: 'post',
        dataType: 'json',
        data: {'to_id':CurUser_id, 'from_id':ClickUser_id, 'message':message.text},
        success: function(result){
          // lưu message thành công thì sẽ gán dữ liệu đã lưu vào class chatlist
          if(result.to_id == CurUser_id){
            $(".chatlist").append(
              `<li class='right clearfix'>
                 <span class='chat-img pull-right'>
                  <img src="//placehold.it/50/FA6F57/fff&amp;text=ME"
                  alt="User Avatar" class="img-circle">
                 </span>
                 <div class='chat-body clearfix'>
                    <div class='header'>
                      <small class=' text-muted'>
                        <span class='glyphicon glyphicon-time'></span>
                        <span class='send_at'>${result.created}</span>
                      </small>
                      <strong class='pull-right primary-font username'>
                        ${result.username_to}
                      </strong>
                    </div>
                    <p class='message'>
                      ${result.message}
                    </p>
                 </div>
              </li>`);
            document.getElementById('message').value = ''
          }
          // lắng nghe server gửi message về client

        },
        error: function(err){
        }
      });
    }
    // updatemessage từ server gửi qua

  });
  socket.on('server_send_mesage', function (message) {
    updatemessage(message);
  });
  function updatemessage(message){
    $("#message").html('');
    $(".chatlist").append(
      `<li class='left clearfix'>
        <span class='chat-img pull-left'>
          <img src='//placehold.it/50/55C1E7/fff&amp;text=STAFF' alt='User Avatar' class='img-circle'>
        </span>
        <div class='chat-body clearfix'>
            <div class='header'>
                <strong class='primary-font username'>${message.username}</strong>
                  <small class='pull-right text-muted'>
                    <span class='glyphicon glyphicon-time'></span>
                    <span class='send_at'>${message.created}</span>
                  </small>
            </div>
            <p class='message'>${message.text}</p>
        </div>
      </li>`)
  }
});
