<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->model('user_model');

	}
	public function index() {
		$list = $this->user_model->hienthi();
		$this->data['list'] = $list;
		$this->load->view('chatmessage/index',$this->data);
	}
	public function login(){
		$username = $this->input->post('username');

		$list = $this->user_model->hienthi2($username);

		if($list != null){
			$this->session->set_userdata('user_info',$list);
			redirect('chat');
		}

		$this->data['cur_user'] = $username;

	$this->load->view('login/login');
}
	public function addRom(){
		if($this->input->post()){
			$data = [
				'room_id1'=>$this->input->post('room_id1'),
				'room_id2'=>$this->input->post('room_id2'),
				'room_chat'=>$this->input->post('room_chat')
			];
			$this->user_model->insert($data);
		}
	}

}
