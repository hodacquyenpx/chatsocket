<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {
	function __construct(){
		parent::__construct();

		$this->load->model('user_model');
		$this->load->model('roomchat');
		$this->load->model('message');

	}
	public function index() {
		$list = $this->user_model->hienthi();
		$this->data['list'] = $list;
		$this->load->view('chatmessage/index',$this->data);
	}
	public function login(){
		$username = $this->input->post('username');

		$list = $this->user_model->hienthi2($username);

		if($list != null){
			$this->session->set_userdata('user_info',$list);
			$this->session->set_userdata('status',on);
			redirect('chat');

		}

		$this->data['cur_user'] = $username;

	$this->load->view('login/login');
}
	public function addRoom(){
		if($this->input->post()){
			$data = [
				'room_id1'=>$this->input->post('room_id1'),
				'room_id2'=>$this->input->post('room_id2'),
				'roomchat'=>$this->input->post('room_chat')
			];

			if($this->roomchat->insert($data)){
				$myObj['status'] = 1;
				$myObj['room_id1'] = $this->input->post('CurUser_id');
				$myObj['room_id2'] = $this->input->post('ClickUser_id');
				$myObj['roomchat'] = $this->input->post('room_chat');
			} else {
				$myObj['status'] = 0;
			}
			echo json_encode($myObj);
		}
	}

	public function checkRoom(){
		if($this->input->post()){
			$to_id = $this->input->post('room_id1');
			$from_id = $this->input->post('room_id2');
			if($to_id >= $from_id){
				$curRoom = $this->roomchat->hienthi2($from_id,$to_id);
			}else{
				$curRoom = $this->roomchat->hienthi2($to_id,$from_id);
			}
			if($curRoom){

				$list_mess = $this->message->hienthi2($curRoom->id);
				$myObj['status'] = 1;
				$myObj['room_id1'] = $this->input->post('room_id1');
				$myObj['room_id2'] = $this->input->post('room_id2');
				$myObj['roomchat'] = $this->input->post('room_chat');
				$myObj['list_mess'] = $list_mess;
			} else{
				$myObj['status'] = 0;
			}
			echo json_encode($myObj);
		}
	}


	public function addMess(){
		if($this->input->post()){
			$to_id = $this->input->post('to_id');
			$from_id = $this->input->post('from_id');
			$CurUsername = $this->user_model->checkuser($to_id);
			$ClickUsername = $this->user_model->checkuser($from_id);
			if($to_id >= $from_id){
				$list = $this->roomchat->hienthi2($from_id,$to_id);
			}else{
				$list = $this->roomchat->hienthi2($to_id,$from_id);
			}
			$message = $this->input->post('message');
			$created = date('Y-m-d H:i:s');
			$data = [
				'id_roomchat' => $list->id,
				'to_id' => $to_id,
				'from_id' => $from_id,
				'username_to' =>$CurUsername->username,
				'username_from' =>$ClickUsername->username,
				'message' => $message,
				'created' => $created
			];
			if($this->message->insert($data)){
				$myObj['status'] = 1;
				$myObj['to_id'] = $to_id;
				$myObj['from_id'] = $from_id;
				$myObj['username_to'] = $CurUsername->username;
				$myObj['username_from'] = $ClickUsername->username;
				$myObj['message'] = $message;
				$myObj['created'] = $created;
			} else {
				$myObj['status'] = 0;
			}
			echo json_encode($myObj);
		}
	}
}
